﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Camera))]
public class CameraShake : MonoBehaviour
{
    private Vector3 initialPosition;

    private System.Random randX;
    private System.Random randY;

    private bool shaking;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        this.initialPosition = this.transform.localPosition;

        this.randX = new System.Random();
        this.randY = new System.Random();

        this.shaking = false;
        this.timer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (shaking)
        {
            timer += Time.deltaTime;

            Vector3 newPos = new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(-0.2f, 0.2f), 0f);
            this.transform.localPosition = initialPosition + newPos;

            if (timer >= 0.2f)
            {
                shaking = false;
                this.transform.localPosition = initialPosition;
            }
        }
    }

    public void ShakeCamera()
    {
        this.shaking = true;
        this.timer = 0f;
    }

}
