﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class MoveScript : MonoBehaviour
{
    private Rigidbody myRigidBody;
    private float moveSpeed;

    private void Awake()
    {
        this.myRigidBody = this.GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        this.moveSpeed = 15f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!myRigidBody)
        {
            Debug.LogError("No rigid body, bruh");
        }

        float hMovement = Input.GetAxis("Horizontal");
        float vMovement = Input.GetAxis("Vertical");

        Vector3 moveForce = Vector3.zero;

        moveForce.x = hMovement * moveSpeed * Time.deltaTime;
        moveForce.z = vMovement * moveSpeed * Time.deltaTime;

        myRigidBody.AddForce(moveForce, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        FindObjectOfType<CameraShake>().ShakeCamera();
    }
}
